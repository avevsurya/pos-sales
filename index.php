<?php
session_start();

//define("SITE_HOST", "http://".$_SERVER['HTTP_HOST']."/kasir/");//.HOST_URI);
// error_reporting(1);
include 'config.php';   //  echo $_SESSION['username']; exit();
include 'function.php';
if (!empty($_SESSION["username"]) && !empty($_SESSION['password'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="keywords"
              content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="#" type="assets/image/png">

        <title>Point Of Sales | v1.1</title>
        <!--icheck-->

        <!-- data picker -->

        <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/datepicker-custom.css"/>
        <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-timepicker/css/timepicker.css"/>
        <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker.css"/>
        <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css"/>
        <link rel="stylesheet" type="text/css" href="assets/js/toastr/toastr.min.css"/>

        <!-- data picker end -->

        <!-- cart jancuk -->
        <link href="assets/js/iCheck/skins/square/square.css" rel="stylesheet">
        <link href="assets/js/iCheck/skins/square/red.css" rel="stylesheet">
        <link href="assets/js/iCheck/skins/square/blue.css" rel="stylesheet">

        <!--dashboard calendar-->
        <link href="assets/css/clndr.css" rel="stylesheet">

        <!--file upload-->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/js/morris-chart/morris.css">

        <!--common-->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">

        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <!--[endif]-->
    </head>

    <body class="sticky-header">

    <section>
        <!-- left side start-->
        <?php include("sidebar.php"); ?>
        <!-- left side end-->

        <!-- main content start-->
        <div class="main-content">
            <?php include("header.php"); ?>
            <!--body wrapper start-->
            <?php
            // include 'menuatas-member.php';
            if (isset($_GET['hal']) && strlen($_GET['hal']) > 0) {
                $hal = str_replace(".", "/", $_GET['hal']) . ".php";
            } else {
                $hal = "dashboard.php";
            }
            if (file_exists($hal)) {
                include($hal);
            } else {
                include("dashboard.php");
            }
            ?>
            <!--body wrapper end-->
            <!--footer section start-->
            <?php include("footer.php"); ?>
            <!--footer section end-->
        </div>
        <!-- main content end-->
    </section>

    <!-- Placed js at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-1.10.2.min.js"></script>
    <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>

    <script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
    <script src="assets/js/easypiechart/easypiechart-init.js"></script>

    <script src="assets/js/sparkline/jquery.sparkline.js"></script>
    <script src="assets/js/sparkline/sparkline-init.js"></script>

    <script src="assets/js/iCheck/jquery.icheck.js"></script>
    <script src="assets/js/icheck-init.js"></script>

    <script src="assets/js/flot-chart/jquery.flot.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.tooltip.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.resize.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.pie.resize.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.selection.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.stack.js"></script>
    <script src="assets/js/flot-chart/jquery.flot.time.js"></script>
    <script src="assets/js/main-chart.js"></script>
    <script src="assets/js/jquery-1.10.2.min.js"></script>
    <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.25/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.25/datatables.min.js"></script>

    <script src="assets/js/editable-table.js"></script>

    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/validation-init.js"></script>

    <script type="text/javascript" src="assets/js/toastr/toastr.min.js"></script>

    <!-- cart -->
    <?php
    if ($_GET['hal'] == 'dashboard' || $_GET['hal'] == 'master/laporan_transaksi/list' || $_GET['hal'] == 'master/laporan_product/list') {
        ?>
        <script src="assets/js/morris-chart/morris.js"></script>
        <script src="assets/js/morris-chart/raphael-min.js"></script>
        <!--
        <script src="assets/js/calendar/clndr.js"></script>
        <script src="assets/js/calendar/evnt.calendar.init.js"></script>
        <script src="assets/js/calendar/moment-2.2.1.js"></script>
        -->
        <script src="assets/js/underscore-min.js"></script>
        <!-- <script src="assets/js/scripts.js"></script>  -->
        <script src="assets/js/dashboard-chart-init-edi.js"></script>
        <!--Dashboard Charts-->

        <?php
    }
    ?>
    <!-- cart -->
    <script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/js/lightbox.min.css"/>
    <script src="assets/js/pickers-init.js"></script>
    <script src="assets/js/scripts.js"></script>
    <script src="assets/js/jquery.twbsPagination.min.js"></script>
    <!--file upload-->
    <script type="text/javascript" src="assets/js/bootstrap-fileupload.min.js"></script>
    <!-- <script src="assets/js/edi.js"></script> -->
    <script>
        jQuery(document).ready(function () {
            $('#date-picker').daterangepicker({
                locale: {
                  format: 'YYYY-MM-DD'
                },
            });

            $('#date-picker').on('apply.daterangepicker', function(ev, picker) {
              var startDate = picker.startDate.format('YYYY-MM-DD');
              var endDate = picker.endDate.format('YYYY-MM-DD');

              window.location = $(location).attr('href') + '&startDate=' + startDate + '&endDate=' + endDate;
            });
        });
    </script>


<script>
    $(document).ready(function() {
    var oTable = $('.product-list').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": {
            "url": "master/product/data.php?action=ajax",
            "type": "POST",
            "data": function(data) {
                data.status = $('#product-list-stock').val();
            },		
        },
        "columns": [
            { "data": "image", render: function (data, type, full, meta) { return '<img src="'+data+'" />' }},
            { "data": "product_name" }, 
            { "data": "product_price" },
            { "data": "product_price_hpp" },
            { "data": "product_stock" },
            { "data": "product_desc" },
            { "data": "action" }
        ]
		});
	
        $('#product-list-stock').change(function () {
            oTable.ajax.reload();
        });

        var stockTable = $('.product-stock-list').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": {
            "url": "master/laporan_stock/data.php?action=ajax",
            "type": "POST",
            "data": function(data) {
                data.status = $('#product-list-stock').val();
            },		
        },
        "columns": [
            { "data": "image", render: function (data, type, full, meta) { return '<img src="'+data+'" />' }},
            { "data": "product_name" }, 
            { "data": "product_price" },
            { "data": "product_price_hpp" },
            { "data": "product_desc" },
            { "data": "product_stock" },
        ]
		});
	
        $('#product-list-stock').change(function () {
            stockTable.ajax.reload();
        });

        $('#catalog-pagination').twbsPagination({
        totalPages: $('.total-halaman').val() > 0 ? $('.total-halaman').val() : 1,
        visiblePages: 7,
        startPage: parseInt($('.halaman').val()),
        initiateStartPageClick:false,
        onPageClick: function (event, page) {
            window.location.replace('/index.php?hal=master/product/catalog&page=' + page);
        }
        });

        $('.catalog-search').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                window.location.replace('/index.php?hal=master/product/catalog&page=1&search=' + $(this).val());
            }
        });

        $('#catalog-stock').on('change', function() {
            window.location.replace('/index.php?hal=master/product/catalog&page=1&status=' + $(this).val());
        });

        lightbox.option({
        'resizeDuration': 0,
        'wrapAround': true
        });

        var transactionTable = $('.transaction-list').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": {
            "url": "master/transaksi/data.php?action=ajax",
            "type": "POST",
            "data": function(data) {
                console.log(data)
            },		
        },
        "columns": [
            { "data": "id_orders" }, 
            { "data": "nama_petugas" },
            { "data": "tgl_order" },
            { "data": "qty" },
            { "data": "total" },
            { "data": "action" }
        ]
		});
});	
</script>

    </body>
    </html>
    <?php
} else {
    header("Location: login.php");
}
?>