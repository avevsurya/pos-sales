// Use Morris.Area instead of Morris.Line
/*Morris.Donut({
    element: 'graph-donut',
    data: [
        {value: 40, label: 'New Visit', formatted: 'at least 70%' },
        {value: 30, label: 'Unique Visits', formatted: 'approx. 15%' },
        {value: 20, label: 'Bounce Rate', formatted: 'approx. 10%' },
        {value: 10, label: 'Up Time', formatted: 'at most 99.99%' }
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (x, data) { return data.formatted; }
});
*/


/*STATISTIK HARIAN*/
if($('#graph-area1').length > 0) {
    Morris.Area({
        parseTime: false,
        element: 'graph-area1',
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        fillOpacity:.7,
        data: transDayStat,
        lineColors:['#ff8673'],
        xkey: 'period',
        ykeys: ['Rp', 'Transaksi'],
        labels: ['Rp', 'Jumlah Transaksi'],
        pointSize: 3,
        lineWidth: 1,
        hideHover: 'auto',

    });
}

if($('#graph-area2').length > 0) {
    Morris.Area({
        element: 'graph-area2',
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        fillOpacity:.7,
        data: transMonthStat, //var ppobStat di page home 
        lineColors:['#ff8673'],
        xkey: 'period',
        ykeys: ['Rp', 'Transaksi'],
        labels: ['Rp', 'Jumlah Transaksi'],
        pointSize: 3,
        lineWidth: 1,
        hideHover: 'auto',
    });
}

if($('#graph-area3').length > 0) {
    Morris.Area({
        element: 'graph-area3',
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        fillOpacity:.7,
        data: transMonthStats, //var ppobStat di page home 
        lineColors:['#ff8673'],
        xkey: 'period',
        ykeys: ['Rp', 'Transaksi'],
        labels: ['Rp', 'Jumlah Transaksi'],
        pointSize: 3,
        lineWidth: 1,
        hideHover: 'auto',
    });
}

if($('#graph-area4').length > 0) {
    Morris.Bar({
        element: 'graph-area4',
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        parseTime:false,
        fillOpacity:.7,
        data: transProductStats, //var ppobStat di page home 
        lineColors:['#ff8673'],
        xkey: 'name',
        ykeys: ['Jumlah'],
        labels: ['Jumlah'],
        pointSize: 3,
        lineWidth: 1,
        hideHover: 'auto',
    });
}