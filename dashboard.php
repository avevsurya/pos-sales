<?php
error_reporting(0);

$begin = new DateTime(date('Y-m-01'));
$end   = new DateTime(date('Y-m-t'));

$month_order = [];
$queryRowOrder = mysql_query("select date(tgl_order) as tgl_order, sum(od.subtotal) as total, sum(od.jumlah) as qty from orders o join orders_detail od on o.id_orders = od.id_orders where month(o.tgl_order) = month(current_date()) and year(o.tgl_order) = year(current_date()) group by day(tgl_order)");

while($res = mysql_fetch_assoc($queryRowOrder)) {
    $month_order[] = $res;
}

$day_order = [];
$queryRowOrderday = mysql_query("select DATE_FORMAT(tgl_order, '%H') as time, sum(od.subtotal) as total, sum(od.jumlah) as qty from orders o join orders_detail od on o.id_orders = od.id_orders where date(tgl_order) = current_date() group by DATE_FORMAT(tgl_order, '%H')");

while($res = mysql_fetch_assoc($queryRowOrderday)) {
    $day_order[] = $res;
}

function searcharray($value, $key, $array) {
   foreach ($array as $k => $val) {
       if ($val[$key] == $value) {
           return $k;
       }
   }
   return null;
}

$hours = [
    '00',
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',    
];

?>
<div class="wrapper">
    <div class="row states-info">

        <a href="?hal=pos" style="color: #fff;">
            <div class="col-md-3">
                <div class="panel red-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title"> Point of Sale</span>
                                <h4>POS</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="?hal=master/category/list" style="color: #fff;">
            <div class="col-md-3">
                <div class="panel blue-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-tag"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title"> Kategori Product </span>
                                <h4>Kategori</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="?hal=master/product/list" style="color: #fff;">
            <div class="col-md-3">
                <div class="panel green-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">Data  Product  </span>
                                <h4>Product</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="?hal=master/user/list" style="color: #fff;">
            <div class="col-md-3">
                <div class="panel yellow-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">Data User  </span>
                                <h4>User</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </a>

    <div class="row">

        <div class="col-sm-6">
            <section class="panel">
                <header class="panel-heading">
                    TRANSAKSI DAY : <span style="color:#FF6600;"><?php echo date('d F Y'); ?></span>
                    <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                </header>
                <div class="panel-body">
                    <div id="graph-area1"></div>

                    <script>
                        var transDayStat = [
                            <?php foreach($hours as $hour) : ?>
                                <?php $index = searcharray($hour, 'time', $day_order); ?>

                                {
                                    period: '<?= $hour ?>:00',
                                    Rp: <?= is_int($index) ? $day_order[$index]['total'] : 0 ?>,
                                    Transaksi: <?= is_int($index) ? $day_order[$index]['qty'] : 0 ?>
                                }, 
                            <?php endforeach; ?>
                        ];
                    </script>

                </div>
            </section>
        </div>

        <div class="col-sm-6">
            <section class="panel">
                <header class="panel-heading">
                    TRANSAKSI MONTH : <span style="color:#FF6600;"><?php echo date('F Y'); ?></span>
                    <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                </header>
                <div class="panel-body">
                    <div id="graph-area2"></div>
                    <script>
                        var transMonthStat = [<?php for($i = $begin; $i <= $end; $i->modify('+1 day')) { ?> 
                            {
                                <?php 
                                    $index = searcharray($i->format("Y-m-d"), 'tgl_order', $month_order);
                                ?>

                                period: '<?= $i->format("Y-m-d") ?>',
                                Rp: <?= is_int($index) ? $month_order[$index]['total'] : 0 ?>,
                                Transaksi: <?= is_int($index) ? $month_order[$index]['qty'] : 0 ?>
                            }, 

                            <?php } ?>
                       ];
                    </script>


                    <script>

                    </script>
                </div>
            </section>
        </div>


    </div>

</div>