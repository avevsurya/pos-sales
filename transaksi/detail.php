<style type="text/css">
    .printable {
        height: 0px;
        overflow: hidden;
    }
    
    @media print {
        .wrapper, .header-section, .left-side, footer {
            display: none;
        }   

        .printable {
            height: auto;
        }

        body { overflow: hidden; }

        .sticky-header .main-content {
            margin-left: 0;
            padding-top: 0;
        }
    }
</style>

<?php

error_reporting(0);
$id = $_GET['id'];
$queryRowOrder = mysql_query("SELECT *
FROM
    `product`
    INNER JOIN `orders_detail` 
        ON (`product`.`product_id` = `orders_detail`.`product_id`)
    INNER JOIN `orders` 
        ON (`orders`.`id_orders` = `orders_detail`.`id_orders`) WHERE orders.id_orders= '" . $id . "'");

?>
<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12 text-left">
            <section class="panel">
                <header class="panel-heading">
                    Detail Transaksi Order
                </header>

                <div class="panel-body">
                    <?php
                    $qOrder = mysql_query("SELECT * FROM orders WHERE id_orders ='" . $id . "'");
                    $dataOrder = mysql_fetch_array($qOrder);
                    
                    ?>
                    Nama Petugas : <b><?php echo $dataOrder['nama_petugas']; ?></b><br>
                    Tanggal :
                    <b>
                        <?php

                        echo $dataOrder['tgl_order'] . "";
                        ?>
                    </b>
                    <table class="table">
                        <thead>
                        <tr>
                            <td>No</td>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Diskon</th>
                            <th>Qty</th>
                            <th>Unit</th>
                            <th>Sub Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $total = 0;

                        while ($data = mysql_fetch_array($queryRowOrder)) {
                            $sub_total = +($data['product_price'] * $data['jumlah']) - $data['discount'];
                            $total += $sub_total;
                            ?>
                            <tr>
                                <td>
                                    <?php echo $no++; ?>
                                </td>
                                <td>
                                    <?php echo $data['product_name'] ?></td>
                                <td>Rp. <?php echo number_format($data['product_price'], 0, ',', '.'); ?></td>
                                <td>Rp. <?php echo number_format($data['discount'], 0, ',', '.'); ?></td>
                                <td><?php echo $data['jumlah']; ?></td>
                                <td><?php echo $data['product_unit']; ?></td>
                                <td>Rp. <?php echo number_format($sub_total, 0, ',', '.'); ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="6">
                                Total
                            </td>
                            <td>Rp.
                                <?php echo number_format($total, 0, ',', '.'); ?>
                            </td>
                        </tr>
                        <tr>
                            <!-- end modal -->
                        </tr>
                        </tbody>
                    </table>

                     <button class="btn btn-primary" type="submit" onclick="window.print();">
                            <i class="fa fa-print"></i> print
                    </button>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

<link rel="stylesheet" type="text/css" href="assets/css/print.css?a=2"/>

<div class="printable">
    <?php
        $sql = mysql_query("SELECT * FROM store WHERE id = 1");
        $store = mysql_fetch_array($sql);

        $queryRowOrder = mysql_query("SELECT *
        FROM
            `product`
            INNER JOIN `orders_detail` 
                ON (`product`.`product_id` = `orders_detail`.`product_id`)
            INNER JOIN `orders` 
                ON (`orders`.`id_orders` = `orders_detail`.`id_orders`) WHERE orders.id_orders= '" . $id . "'");
        ?>

    <page size="A4">
        <div class="header">
            <div style="width: 300px;line-height: 1em;">
               
                <div style="float: left;text-align: left;">
                    <?= $store['name'] ?><BR>
                    <?= $store['street'] ?><BR>
                    <?= $store['city'] ?> <?= $store['province'] ?><BR>
                    Phone : <?= $store['phone'] ?>
                </div>  
            </div>
            <div style="float: right;">
                <p>Kepada :</p>
                <p>________________________</p>
            </div>
            <div style="clear: both"></div>
            <hr>

            <p>No Nota : <?= $id ?></p>
            <table cellpadding="1" class="product-table">
                <thead>
                    <th width="5%">No</th>
                    <th width="35%">Nama Produk</th>
                    <th width="10%" style="text-align: right;">Harga</th>
                    <th width="5%" style="text-align: center">Qty</th>
                    <th width="10%" style="text-align: center">Unit</th>
                    <th width="15%" style="text-align: right;">Diskon</th>
                    <th width="20%" style="text-align: right;">Subtotal</th>
                </thead>
                
                <?php $sub_total = 0;$total = 0; $no = 1;?>

                <?php while ($data = mysql_fetch_array($queryRowOrder)) {
                    $sub_total = +($data['product_price'] * $data['jumlah']) - $data['discount'];
                    $total += $sub_total;
                    ?>

                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['product_name']; ?></td>
                        <td style="text-align: right;">Rp. <?php echo number_format($data['product_price'], 0, ',', '.'); ?></td>
                        <td style="text-align: center"><?php echo $data['jumlah'] ?></td>
                        <td style="text-align: center"><?php echo $data['product_unit'] ?></td>
                        <td style="text-align: right;"><?= $data['discount'] > 0 ? 'Rp.' . number_format($data['discount'], 0, ',', '.') : 0 ?></td>
                        <td style="text-align: right;">Rp. <?php echo number_format(($data['jumlah'] * $data['product_price']) - $data['discount'], 0, ',', '.'); ?></td>
                    </tr>
                <?php } ?>

                <tfoot style="border-top: 1px dashed #ccc;">
                    <td></td>
                    <td><strong>TOTAL</strong></td>
                    <td></td>
                    <td style="text-align: center;"><?= $itemcetak ?></td>
                    <td></td>
                    <td style="text-align: right;"><?= $totaldiskon > 0 ? 'Rp.' . number_format($totaldiskon, 0, ',', '.') : 0 ?></td>
                    <td colspan="3" style="text-align: right;">Rp. <?php echo number_format($total, 0, ',', '.'); ?></td>
                </tfoot>
            </table>
        </div>  
        <table width="100%" class="signature">
            <tr>
                <td>
                    Pelanggan

                    <p>( .............................. )</p>
                </td>
                <td>
                    Disetujui

                    <p>( .............................. )</p>
                </td>
                <td>
                    Disiapkan

                    <p>( .............................. )</p>
                </td>
            </tr>
        </table>
        <p style="text-align: center;font-size: 10px;margin-top: 20px;">
            ***************<?php echo $dataOrder['tgl_order'] . ""; ?>**************<BR>
            BARANG YANG SUDAH DIBELI
            TIDAK DAPAT DITUKAR/DIKEMBALIKAN    
        </p>

    </page>
</div>