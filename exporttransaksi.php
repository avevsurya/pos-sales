<?php
        require_once('config.php');

        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=laporan_transaksi.csv');  

        $output = fopen("php://output", "w");  
        fputcsv($output, array('ID Order', 'Tanggal Order', 'Petugas', 'Jumlah Item', 'Total'));  

        $begin = new Datetime(date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d')))));
        $end   = new Datetime(date('Y-m-d'));

        if(isset($_GET['startDate']) && isset($_GET['endDate'])) {
            $begin = new Datetime($_GET['startDate']);
            $end   = new Datetime($_GET['endDate']);
        }

        $queryTransaksiCSV = mysql_query("SELECT orders.id_orders, date(tgl_order) as tgl_order, nama_petugas, SUM(orders_detail.jumlah) AS qty,  SUM(orders_detail.subtotal) AS total
                                                 FROM
                                                    `orders` join orders_detail on orders.id_orders = orders_detail.id_orders
                                                   where date(tgl_order) >= '".$begin->format('Y-m-d')."' and date(tgl_order) <= '".$end->format('Y-m-d')."' group by date(tgl_order) ORDER BY orders.id_orders ASC");

        while($row = mysql_fetch_assoc($queryTransaksiCSV))  
        {  
             fputcsv($output, $row);  
        }  

        fclose($output);  
?>