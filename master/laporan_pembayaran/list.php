<?php
    include 'config.php'
?>
<!--body wrapper start-->
<script type="text/javascript">
    function myFunction() {
    var x = document.getElementById("nonota");
     var keycode = (event.keyCode ? event.keyCode : event.which);
     console.log(keycode);
    x.value = x.value.toUpperCase();
    if(keycode == '13'){
        if (x.value !=""){
        window.location.replace('index.php?hal=master/laporan_pembayaran/list&search='+x.value);
    }else{
       window.location.replace('index.php?hal=master/laporan_pembayaran/list'); 
    }
    }
}
</script>
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-lg-9">
                           Rekap Laporan Pembayaran    
                        </div>
                        <div class="col-lg-3">
                                <div class="input-group">
                                <input onkeyup="myFunction()" id="nonota"placeholder="nomor nota" type="text" class="form-control">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class=" form">

                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered product-stock-list" id="editable-sample">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" width="20px">No</th>
                                    <th style="text-align: center;" width="90px">No Nota</th>
                                    <th style="text-align: center;"width="100px">Nama Customer</th>
                                    <th style="text-align: center;" width="90px">Tanggal Order</th>
                                    <th style="text-align: center;" width="90px">Status</th>
                                    <th style="text-align: center;" width="40px">QTY</th>
                                    <th style="text-align: center;" width="80px">Pembayaran</th>
                                    <th style="text-align: center;" width="80px">Jumlah Tagihan</th>
                                    <th style="text-align: center;" width="50px">Lihat Rincian</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php 
                                        $batas = 15;
                                        $halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
                                        $search = isset($_GET['search'])? "WHERE o.id_orders ='".$_GET['search']."'" : "";
                                        $halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;    
 
                                        $previous = $halaman - 1;
                                        $next = $halaman + 1;

                                      $query = mysql_query("select o.id_orders, nama_petugas, tgl_order, kepada, status, nominal_pembayaran, sum(jumlah) qty, sum(subtotal) total from orders o join orders_detail od on o.id_orders = od.id_orders $search group by o.id_orders order by id_orders DESC");
                                      $jumlahdata = mysql_num_rows($query);
                                      $total_halaman = ceil($jumlahdata / $batas);

                                      $datapagination = mysql_query("select o.id_orders, nama_petugas, tgl_order, kepada, status, nominal_pembayaran, sum(jumlah) qty, sum(subtotal) total from orders o join orders_detail od on o.id_orders = od.id_orders $search group by o.id_orders order by id_orders desc LIMIT $halaman_awal, $batas");

                                      $no = $halaman_awal + 1;
                                      $jumlahnumber = 2;

                                      while($row = mysql_fetch_array($datapagination)){

                                        $id_orders = $row['id_orders'];

                                        $daftarproduk = mysql_query("SELECT * FROM orders_detail,product 
                                     WHERE orders_detail.product_id=product.product_id 
                                     AND id_orders='$id_orders'");

                                        $total = 0;
                                        $total_discount=0;
                                            while ($data = mysql_fetch_array($daftarproduk)) {
                                            $discount = ($data['harga_barang']/100) * $data['discount'];
                                            $sub_total =+ (($data['harga_barang']-$discount) * $data['jumlah']);
                                            $total += $sub_total;
                                            $total_discount = $data['total_discount'];

                                            }
                                             $total = $total-(($total/100) * $total_discount);

                                        $totalnominal = $row['total'];
                                        $nominalpembayaran = $row['nominal_pembayaran'];

                                  ?>

                                <tr>
                                    <td style="text-align: center;"><?php echo $no++ ?></td>
                                    <td style="text-align: center;"><?php echo $row['id_orders'] ?></td>
                                    <td style="text-align: center;"><?php echo $row['kepada'] ?></td>
                                    <td style="text-align: center;"><?php echo $row['tgl_order'] ?></td>
                                    <td style="text-align: center;"><?php if ($row['status'] == 'Piutang') { ?>
                                        <span class="badge badge-warning"><?php echo $row['status'] ?></span>
                                    <?php
                                    } else if ($row['status'] == 'Lunas') { ?>
                                        <span class="badge badge-success"><?php echo $row['status'] ?></span>
                                    <?php
                                    }
                                    ?> </td>
                                    <td style="text-align: center;"><?php echo $row['qty'] ?></td>
                                    <td style="text-align: right;"><?php echo "Rp. ".number_format($nominalpembayaran,0,',','.') ?></td>
                                    <td style="text-align: right;"><?php echo "Rp . ".number_format($total,0,',','.') ?></td>
                                    <td style="text-align: center;">
                                         <a href="?hal=master/transaksi/detail-pembayaran&id=<?php echo $row['id_orders']; ?>">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-eye"></i> Lihat</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination justify-content-center">
                                <?php
                                      $start_number = ($halaman > $jumlahnumber)? $halaman - $jumlahnumber : 1;
                                      $end_number = ($halaman < ($total_halaman - $jumlahnumber))? $halaman + $jumlahnumber : $total_halaman;
                                                                           
                                      if($halaman == 1){
                                        echo '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
                                        echo '<li class="page-item disabled"><a class="page-link" href="#"><span aria-hidden="true">&laquo;</span></a></li>';
                                      } else {
                                        $link_prev = ($halaman > 1)? $halaman - 1 : 1;
                                        echo '<li class="page-item"><a class="page-link" href="index.php?hal=master/laporan_pembayaran/list">First</a></li>';
                                        echo '<li class="page-item"><a class="page-link" href="index.php?hal=master/laporan_pembayaran/list&halaman='.$link_prev.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                      }
                                 
                                      for($i = $start_number; $i <= $end_number; $i++){
                                        $link_active = ($halaman == $i)? ' active' : '';
                                        echo '<li class="page-item '.$link_active.'"><a class="page-link" href="index.php?hal=master/laporan_pembayaran/list&halaman='.$i.'">'.$i.'</a></li>';
                                      }
                                 
                                      if($halaman == $total_halaman){
                                        echo '<li class="page-item disabled"><a class="page-link" href="#"><span aria-hidden="true">&raquo;</span></a></li>';
                                        echo '<li class="page-item disabled"><a class="page-link" href="#">Last</a></li>';
                                      } else {
                                        $link_next = ($halaman < $total_halaman)? $halaman + 1 : $total_halaman;
                                        echo '<li class="page-item"><a class="page-link" href="index.php?hal=master/laporan_pembayaran/list&halaman='.$link_next.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                                        echo '<li class="page-item"><a class="page-link" href="index.php?hal=master/laporan_pembayaran/list&halaman='.$total_halaman.'">Last</a></li>';
                                      }
                                    ?>
                            </ul>
                        </nav>
                    </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>