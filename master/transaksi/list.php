<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Data Transaksi
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header>
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <div class="space15"></div>
                        <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered transaction-list" id="editable-sample">
                            <thead>
                            <tr>
                                <th width="20%">ID Order</th>
                                <th width="20%">Nama Petugas</th>
                                <th width="15%">Tanggal</th>
                                <th width="15%">Qty</th>
                                <th width="15%">Total</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<style>
    .dataTables_filter {
display: none;
}
</style>