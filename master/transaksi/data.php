<?php
  session_start();
  ini_set('display_errors', '1');
  error_reporting(E_ALL);

  include '../../config.php';

  if (empty($_SESSION["username"]) && empty($_SESSION['password'])) {
    header("Location: login.php");
  }

  function getTransaction($limit = 10, $keyword, $page, $status) {
    $data = [];

    $allData = mysql_query("SELECT * FROM orders");
    
    while ($r = mysql_fetch_array($allData)) {
      $data[] = $r;
    }

    $totalData = count($data);
    $totalFiltered = count($data);

    $orders = mysql_query("select o.id_orders, nama_petugas, tgl_order, sum(jumlah) qty, sum(subtotal) total from orders o join orders_detail od on o.id_orders = od.id_orders group by o.id_orders order by id_orders desc LIMIT $limit OFFSET $page");

    while ($r = mysql_fetch_array($orders)) {
      $r['action'] = '<a href="?hal=master/transaksi/cetak&id='.$r['id_orders'].'"><button class="btn btn-primary" type="submit"><i class="fa fa-eye"></i> Lihat</button></a>';
      $filtered[] = $r;
    }

    $data = $filtered;

    $data['data'] = $data;
    $data['recordsTotal'] = $totalData;
    $data['recordsFiltered'] = $totalFiltered;
    
    echo json_encode($data);
  }

  if ($_GET['action'] == 'ajax') {
    getTransaction($_POST['length'], $_POST['search']['value'], $_POST['start'], $_POST['status']);
  }