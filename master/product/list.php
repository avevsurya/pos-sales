<?php
if (isset($_GET['hapus'])) {
    $cekGambar = mysql_query("SELECT * FROM product WHERE product_id='".$_GET['hapus']."'");
    $data = mysql_fetch_array($cekGambar);
    if(!empty($data['product_images'])){
        $file = "assets/images/product/".$data['product_images'];
        unlink($file);
    }
    $queryHapus = mysql_query("DELETE FROM product where product_id = '" . $_GET['hapus'] . "'");
    if ($queryHapus) {
        echo "<script> alert('Data Berhasil Dihapus'); location.href='index.php?hal=master/product/list' </script>";
        exit;
    }
}
?>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Data Product
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header>
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <div class="clearfix">
                            <?php if($_SESSION['level'] == 'manager' || $_SESSION['level'] == 'super admin') : ?>
                                <div class="btn-group">
                                    <a href="?hal=master/product/add">
                                        <button data-toggle="modal" class="btn btn-primary">
                                            Add New <i class="fa fa-plus"></i>
                                        </button>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <div class="btn-group pull-right">
                            </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <select name="" id="product-list-stock" class="form-control" style="margin-top:20px">
                                <option value="">All data</option>
                                <option value="ready">Ready stock</option>
                                <option value="out">Out of stock</option>
                            </select>
                        </div>
                        <div class="col-md-9 col-sm-6 col-xs-12">&nbsp;</div>
                        </div>
                        <div class="space15"></div>
                        <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered product-list" id="editable-sample">
                            <thead>
                            <tr>
                                <th width="20%">Foto</th>
                                <th width="20%">Nama</th>
                                <th width="15%">Harga</th>
                                <th width="15%">Harga HPP</th>
                                <th>Stock</th>
                                <th>Deskripsi</th>
                                <?php if($_SESSION['level'] == 'manager' || $_SESSION['level'] == 'super admin') : ?>
                                    <th>Action</th>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>