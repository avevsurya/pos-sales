<?php include 'catalog_data.php'; ?>

<input type="hidden" class="total-halaman" value="<?= $products['total_halaman'] ?>">
<input type="hidden" class="halaman" value="<?= $products['halaman'] ?>">
<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
            <header class="panel-heading">
                    <div class="row">
                        <div class="col-lg-8">
                            Katalog Produk    
                        </div>
                        <div class="col-lg-2">
                          <select name="" id="catalog-stock" class="form-control">
                                <option value="">All data</option>
                                <option value="ready">Ready stock</option>
                                <option value="out">Out of stock</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control catalog-search">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="panel-body shop-items">
                  <div class="row">
                    <?php foreach ($products['data'] as $product) : ?>
                      <div class="col-md-3 col-sm-6">
                        <div class="item">
                        <a href="<?= $product['image'] ? $product['image'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" data-lightbox="<?= $product['product_id'] ?>">
                          <img class="img-responsive" src="<?= $product['image'] ? $product['image'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" alt="">
                        </a>
                        <?php foreach($product['images'] as $key => $val) : ?>
                            <a href="<?= $product['images'][$key] ?>" data-lightbox="<?= $product['product_id'] ?>"></a>
                        <?php endforeach; ?>
                          <div class="item-dtls">
                            <h4><strong><a href="#"><?= $product['product_name'] ?></a></strong></h4>
                            <span class="price">Rp. <?php echo number_format($product['product_price'], 0, ',', '.'); ?></span>
                            <span class="stock">Stok : <?= $product['product_stock'] ?></span>
                            <span class="desc">Deskripsi : <?= $product['product_desc'] ?></span>
                          </div>
                        </div>
                      </div>
                      <?php endforeach; ?>
                  </div>
                </div>

                <div id="catalog-pagination"></div>
            </section>
        </div>
    </div>
</div>

<style>
  .shop-items{
    max-width:1300px;
    margin:10px auto;
    padding:0px 20px;
  }
  .shop-items .item {
    position: relative;
    max-width: 360px;
    height:400px;
    margin: 15px auto;
    padding: 5px;
    text-align: center;
    border-radius: 4px;
    overflow: hidden;
    border:2px solid #eee;
  }
  .shop-items .item img {
    width: 100%;
    height: 200px;
    margin: 0 auto;
    border: 1px solid #eee;
    border-radius: 3px;
  }
  .shop-items .item  .item-dtls h4 {
    margin-top: 13px;
    margin-bottom: 10px;
    text-transform: uppercase;
  }
  .shop-items .item  .item-dtls .price {
    display: block;
    margin-bottom: 13px;
    font-size: 18px;
    margin-top:15px;
  }
  .stock {
    display:block;
    margin-bottom:10px;
    font-size:14px;
  }
  .shop-items .item  .ecom {
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    padding-bottom:10px;
    padding-top: 10px;
    -webkit-transition: all 0.35s ease-in;
    -moz-transition: all 0.35s ease-in;
    -ms-transition: all 0.35s ease-in;
    -o-transition: all 0.35s ease-in;
    transition: all 0.35s ease-in;
  }

  #catalog-pagination {
    align-items: center;
    display: flex;
    justify-content: center;
  }

</style>