<?php
  session_start();
  ini_set('display_errors', '1');
  error_reporting(E_ALL);

  require 'config.php';

  if (empty($_SESSION["username"]) && empty($_SESSION['password'])) {
    header("Location: login.php");
  }

  function getProducts($limit = 10, $keyword, $page, $status) {
    if($page == 1) {
      $offset = $page - 1;
    } else {
      $offset = ($page - 1) * $limit;
    }
    
    $data = [];

    $filtered = [];
    $condition = null;

    if($status == 'ready') {
      $condition = 'product_stock > 0';
    } else if($status == 'out') {
      $condition = 'product_stock <= 0';
    }
    
    $allData = mysql_query("SELECT * FROM product");
    
    while ($r = mysql_fetch_array($allData)) {
      $data[] = $r;
    }

    $totalData = count($data);
    $totalFiltered = count($data);

    if(empty($keyword)) {
      if($condition) {
        $condition = "where " . $condition;
      }

      $query = mysql_query("select p.product_id, product_name, product_price, product_price_hpp, product_stock, product_desc, CONCAT('assets/images/product/', product_images) product_images,
      CONCAT('assets/images/product/', pi.image) image from product p left join product_images pi on p.product_id = pi.product_id $condition group by p.product_id ORDER BY p.product_id DESC LIMIT $limit OFFSET $offset");

      while ($r = mysql_fetch_array($query)) {
        $queryImages = mysql_query("select CONCAT('assets/images/product/', image) image, type from product_images where product_id =" . $r['product_id']);
        $images = [];
        while ($re = mysql_fetch_array($queryImages)) {
          $images[] = $re['image'];
        }

        $r['images'] = $images;
        $filtered[] = $r;
      }

      $data = $filtered;
    } else {
      if($condition) {
        $condition = "and " . $condition;
      }
      
      $all = mysql_query("SELECT count(product_id) FROM product WHERE product_name like '%".$keyword."%'");

      $totalFiltered = mysql_fetch_array($all);
      $totalFiltered = $totalFiltered[0] > 0 ? $totalFiltered[0] : 0;

      $query = mysql_query("select p.product_id, product_name, product_price, product_price_hpp, product_stock, product_desc, CONCAT('assets/images/product/', pi.image) image, CONCAT('assets/images/product/', p.product_images) product_images from product p left join product_images pi on p.product_id = pi.product_id WHERE product_name like '%".$keyword."%' $condition group by p.product_id ORDER BY p.product_id DESC LIMIT $limit OFFSET $offset");

      while ($r = mysql_fetch_array($query)) {
        $queryImages = mysql_query("select CONCAT('assets/images/product/', image) image, type from product_images where product_id =" . $r['product_id']);
        $images = [];
        while ($re = mysql_fetch_array($queryImages)) {
          $images[$re['type']] = $re['image'];
        }

        $r['images'] = $images;
        $filtered[] = $r;
      }

      $data = $filtered;
    }

    $batas = $limit;
    $halaman = $page;
    $halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	

    $previous = $halaman - 1;
    $next = $halaman + 1;
    $total_halaman = ceil($totalFiltered / $batas);

    $data['data'] = $data;
    $data['recordsTotal'] = $totalData;
    $data['recordsFiltered'] = $totalFiltered;
    $data['batas'] = $batas;
    $data['halaman'] = $page;
    $data['halaman_awal'] = $halaman_awal;
    $data['previous'] = $previous;
    $data['next'] = $next;
    $data['total_halaman'] = $total_halaman;
    
    return $data;
  }

  $products = getProducts(24, @$_GET['search'], @$_GET['page'], @$_GET['status']);