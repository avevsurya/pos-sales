<?php error_reporting(0); ?>

<?php
    function searcharray($value, $key, $array) {
       foreach ($array as $k => $val) {
           if ($val[$key] == $value) {
               return $k;
           }
       }
       return null;
    }
?>
<?php
        $limit = 20;
        $begin = new Datetime(date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d')))));
        $end   = new Datetime(date('Y-m-d'));

        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 0;
        }

        if(isset($_GET['startDate']) && isset($_GET['endDate'])) {
            $begin = new Datetime($_GET['startDate']);
            $end   = new Datetime($_GET['endDate']);
        }

        if($page){ 
            $start = ($page - 1) * $limit;
        }else{
            $start = 0;
        }

          $no = $start + 1;
          $queryTransaksi = mysql_query("SELECT *, date(tgl_order) as tgl_order, SUM(orders_detail.jumlah) AS qty,  SUM(orders_detail.subtotal) AS total
                                         FROM
                                            `orders` join orders_detail on orders.id_orders = orders_detail.id_orders
                                           where date(tgl_order) >= '".$begin->format('Y-m-d')."' and date(tgl_order) <= '".$end->format('Y-m-d')."' group by date(tgl_order) ORDER BY orders.id_orders ASC limit " . $start . "," . $limit);
          
          while($res = mysql_fetch_array($queryTransaksi)) {
              $month_order[] = $res;
          }

          $totalData = mysql_query("SELECT *
                                         FROM
                                            `orders` where date(tgl_order) >= '".$begin->format('Y-m-d')."' and date(tgl_order) <= '".$end->format('Y-m-d')."' group by date(tgl_order) ");

          $totalData = mysql_num_rows($totalData);
          $grandtotal=0;
          $totalItem = 0;
?>

<style type="text/css">
    .pagination1 {
    margin:0; 
    padding:0;
    float:left;
    }
    .pagination1 ul {
    width:300px;
    float: right;
    list-style: none;
    margin:0 0 0 ;
    padding:0;
    }
    .pagination1 li span { line-height:45px; font-weight:bold;}
    .pagination1 li {
    margin:0 0 0 0;
    float:left;
    font-size:16px;
    text-transform:uppercase;
    }
    .pagination1 li a {
    color:#7f8588;
    padding:10px 0 0 0; width:33px; height:33px;
    text-decoration:none; text-align:center;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    display:block;
    }
    .pagination1 li:last-child a:hover { background:none; color:#7f8588;}
    .pagination1 li:first-child a:hover { background:none;color:#7f8588;}
    .pagination1 li a:hover {
    color:#fff;
    text-decoration: none;
    display: block;
    padding:10px 0 0 0; width:33px; height:33px;
    }
    .pagination1 li.activepage a { 
    color:#fff;
    text-decoration: none;
    padding: 10px 0 0 0; }
</style>
  <div class="wrapper">
             <div class="row">
                <div class="col-sm-12">
                <section class="panel">
                <header class="panel-heading">
                    Laporan Transaksi
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header>
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group"></div>
                    <div class="btn-group pull-right">
                    
                    </div>
                </div>
                <div class="space15"></div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div id="graph-area3"></div>
                        <script>
                            var transMonthStats = [<?php for($i = $begin; $i <= $end; $i->modify('+1 day')) { ?> 
                            {
                                <?php 
                                    $index = searcharray($i->format("Y-m-d"), 'tgl_order', $month_order);
                                ?>

                                period: '<?= $i->format("Y-m-d") ?>',
                                Rp: <?= is_int($index) ? $month_order[$index]['total'] : 0 ?>,
                                Transaksi: <?= is_int($index) ? $month_order[$index]['qty'] : 0 ?>
                            }, 
                            <?php } ?>
                            ];
                        </script>
                    </div>
                </div>
                <div class="form-group row" style="margin-top: 20px;">
                    <div class="col-sm-4">
                        <?php
                            $begin = new Datetime(date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d')))));
                            $end   = new Datetime(date('Y-m-d'));

                            if(isset($_GET['startDate']) && isset($_GET['endDate'])) {
                                $begin = new Datetime($_GET['startDate']);
                                $end   = new Datetime($_GET['endDate']);
                            }
                        ?>

                        <input type="text" name="daterange" class="form-control" id="date-picker" value="<?= $begin->format('Y-m-d') ?> - <?= $end->format('Y-m-d') ?>" />
                    </div>
                    <div class="col-sm-4">
                        <form method="post" action="exporttransaksi.php">
                                <div class="form-group">
                                        <input type="submit" name="Export" class="btn btn-primary" value="Export CSV"/>
                                </div>                    
                        </form>   
                    </div>
                </div>
                <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>ID Order</th>
                    <th>Tanggal Order</th>
                    <th>Petugas</th>
                    <th>Jumlah Item</th>
                    <th style="text-align: right;">Total</th>
                </tr>
                </thead>
                <tbody>

                <?php
                      foreach($month_order as $rowTransaksi) {

                        $sub_total=+$rowTransaksi['product_price'] * $rowTransaksi['jumlah'];
                   ?>
                <tr class="">
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $rowTransaksi['id_orders']?></td>
                    <td><?php echo $rowTransaksi['tgl_order']?></td>
                    <td><?php echo $rowTransaksi['nama_petugas']?></td>
                    <td>
                        <?php 
                            $queryQTY = mysql_query("SELECT SUM(orders_detail.jumlah) AS jumlahqty , product.product_id
                        FROM
                            `orders`
                            INNER JOIN `orders_detail` 
                                ON (`orders`.`id_orders` = `orders_detail`.`id_orders`)
                            INNER JOIN `product` 
                                ON (`product`.`product_id` = `orders_detail`.`product_id`) WHERE date(orders.tgl_order)='".$rowTransaksi['tgl_order']."' ");
                            $QTY = mysql_fetch_array($queryQTY);
                            $totalItem+=$QTY['jumlahqty'];
                            echo $QTY['jumlahqty'];
                         ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                        $queryTotal=mysql_query("SELECT *
                        FROM
                            `orders`
                            INNER JOIN `orders_detail` 
                                ON (`orders`.`id_orders` = `orders_detail`.`id_orders`)
                            INNER JOIN `product` 
                                ON (`product`.`product_id` = `orders_detail`.`product_id`) WHERE date(orders.tgl_order)='".$rowTransaksi['tgl_order']."'");
                            $totalQuery=0;
                        while($rowQueryTotal=mysql_fetch_array($queryTotal)){

                            $subTotal =+$rowQueryTotal['jumlah'] * $rowQueryTotal['product_price'];
                            $totalQuery +=$subTotal; 
                        }
                        echo "Rp. ".number_format($totalQuery,0,',','.');
                        $total+=$totalQuery;
                        ?>
                    </td>
                </tr>
               <?php } ?>
               <tr>
                   <td>TOTAL</td>
                   <td colspan="3"></td>
                   <td><?= $totalItem ?></td>
                   <td style="text-align: right;"><?php echo "Rp. ".number_format($total,0,',','.') ?></td>
               </tr>
                </tbody>
                </table>
                </div>

                <?php
                    /* Setup page vars for display. */
                    if ($page == 0) $page = 1; //if no page var is given, default to 1.
                    $prev = $page - 1; //previous page is current page - 1
                    $next = $page + 1; //next page is current page + 1
                    $lastpage = ceil($totalData/$limit); //lastpage.
                    $lpm1 = $lastpage - 1; //last page minus 1
                    $targetpage = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    
                    /* CREATE THE PAGINATION */
                    $pagination = "";
                    if($lastpage > 1)
                    { 
                        $pagination .= "<ul class='pagination'>";
                        if ($page > $counter+1) {
                            $pagination.= "<li><a href=\"$targetpage&page=$prev\"><</a></li>"; 
                        }

                        if ($lastpage < 7 + ($adjacents * 2)) 
                        { 
                            for ($counter = 1; $counter <= $lastpage; $counter++)
                            {
                                if ($counter == $page)
                                    $pagination.= "<li><a href='#' class='active'>$counter</a></li>";
                                else
                                    $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
                            }
                        }
                        elseif($lastpage > 5 + ($adjacents * 2)) //enough pages to hide some
                        {
                            //close to beginning; only hide later pages
                            if($page < 1 + ($adjacents * 2)) 
                            {
                                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                                {
                                    if ($counter == $page)
                                        $pagination.= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
                                }
                                $pagination.= "<li>...</li>";
                                $pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
                                $pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>"; 
                            }
                            //in middle; hide some front and some back
                            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                            {
                                $pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
                                $pagination.= "<li><a href=\"$targetpage&page=2\">2</a></li>";
                                $pagination.= "<li>...</li>";
                                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                                {
                                    if ($counter == $page)
                                        $pagination.= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
                                }
                                $pagination.= "<li>...</li>";
                                $pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
                                $pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>"; 
                            }
                            //close to end; only hide early pages
                            else
                            {
                                $pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
                                $pagination.= "<li><a href=\"$targetpage&page=2\">2</a></li>";
                                $pagination.= "<li>...</li>";
                                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; 
                                $counter++)
                                {
                                    if ($counter == $page)
                                        $pagination.= "<li><a href='#' class='active'>$counter</a></li>";
                                    else
                                        $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
                                }
                            }
                        }

                        //next button
                        if ($page < $counter - 1) 
                            $pagination.= "<li><a href=\"$targetpage&page=$next\">></a></li>";
                        else
                            $pagination.= "";
                        $pagination.= "</ul>\n"; 
                    }
                ?>

                <div class="row">
                    <div class="col-md-12 text-center">
                         <?php echo $pagination ?>
                    </div>
                </div>
                </div>
                </div>
                </section>
                </div>
                </div>
        </div>


