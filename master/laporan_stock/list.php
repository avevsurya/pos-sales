<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Data Stock
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header>
                <div class="panel-body">
                    <div class="adv-table editable-table ">
                        <div class="form-group row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <select name="" id="product-list-stock" class="form-control" style="margin-top:20px">
                                <option value="">All data</option>
                                <option value="ready">Ready stock</option>
                                <option value="out">Out of stock</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <form method="post" action="master/laporan_stock/export.php" style="margin-top:20px;">
                                <input type="submit" name="Export" class="btn btn-primary" value="Export CSV"/>
                            </form>
                        </div>
                        <div class="col-md-9 col-sm-6 col-xs-12">&nbsp;</div>
                        </div>
                        <div class="space15"></div>
                        <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered product-stock-list" id="editable-sample">
                            <thead>
                            <tr>
                                <th width="20%">Foto</th>
                                <th width="20%">Nama</th>
                                <th width="15%">Harga</th>
                                <th width="15%">Harga HPP</th>
                                <th>Deskripsi</th>
                                <th>Stock</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>