<?php
        require_once('../../config.php');

        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=laporan_stock.csv');  

        $output = fopen("php://output", "w");  
        fputcsv($output, array('Nama Produk', 'Harga', 'Harga HPP', 'Deskripsi', 'Stok'));  

        $queryStockCSV = mysql_query("select product_name, product_price, product_price_hpp, product_desc, product_stock from product ");

        while($row = mysql_fetch_assoc($queryStockCSV))  
        {  
             fputcsv($output, $row);  
        }

        fclose($output);  
?>