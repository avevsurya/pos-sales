<?php
  session_start();
  ini_set('display_errors', '1');
  error_reporting(E_ALL);

  include '../../config.php';

  if (empty($_SESSION["username"]) && empty($_SESSION['password'])) {
    header("Location: login.php");
  }

  function getProducts($limit = 10, $keyword, $page, $status) {
    $data = [];
    $filtered = [];
    $condition = null;

    if($status == 'ready') {
      $condition = 'product_stock > 0';
    } else if($status == 'out') {
      $condition = 'product_stock <= 0';
    }
    
    if($condition) {
      $allcondition = "where " . $condition;
    }

    $allData = mysql_query("SELECT * FROM product $allcondition");
    
    while ($r = mysql_fetch_array($allData)) {
      $data[] = $r;
    }

    $totalData = count($data);
    $totalFiltered = count($data);

    if(empty($keyword)) {
      if($condition) {
        $condition = "where " . $condition;
      }

      $query = mysql_query("select p.product_id, product_name, product_price, product_price_hpp, product_stock, product_desc, CONCAT('assets/images/product/', product_images) product_images,
      CONCAT('assets/images/product/', pi.image) image from product p left join product_images pi on p.product_id = pi.product_id $condition group by p.product_id ORDER BY p.product_id DESC LIMIT $limit OFFSET $page");

      while ($r = mysql_fetch_array($query)) {
        $filtered[] = $r;
      }

      $data = $filtered;
    } else {
      if($condition) {
        $condition = "and " . $condition;
      }
      
      $all = mysql_query("SELECT count(product_id) FROM product WHERE product_name like '%".$keyword."%'");

      $totalFiltered = mysql_fetch_array($all);
      $totalFiltered = $totalFiltered[0] > 0 ? $totalFiltered[0] : 0;

      $query = mysql_query("select p.product_id, product_name, product_price, product_price_hpp, product_stock, product_desc, pi.image from product p left join product_images pi on p.product_id = pi.product_id WHERE product_name like '%".$keyword."%' $condition group by p.product_id ORDER BY p.product_id DESC LIMIT $limit OFFSET $page");

      while ($r = mysql_fetch_array($query)) {
        $filtered[] = $r;
      }

      $data = $filtered;
    }

    $data['data'] = $data;
    $data['recordsTotal'] = $totalData;
    $data['recordsFiltered'] = $totalFiltered;
    
    echo json_encode($data);
  }

  if ($_GET['action'] == 'ajax') {
    getProducts($_POST['length'], $_POST['search']['value'], $_POST['start'], $_POST['status']);
  }