<?php
        require_once('config.php');

        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=laporan_produk.csv');  

        $output = fopen("php://output", "w");  
        fputcsv($output, array('Nama Produk', 'Qty'));  

        $begin = new Datetime(date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d')))));
        $end   = new Datetime(date('Y-m-d'));

        if(isset($_GET['startDate']) && isset($_GET['endDate'])) {
            $begin = new Datetime($_GET['startDate']);
            $end   = new Datetime($_GET['endDate']);
        }

        $queryTransaksiCSV = mysql_query("select p.product_name, sum(od.jumlah) as qty from orders o join orders_detail od on o.id_orders = od.id_orders join product p on od.product_id = p.product_id WHERE date(tgl_order) >= '".$begin->format('Y-m-d')."' and date(tgl_order) <= '".$end->format('Y-m-d')."' group by p.product_id order by p.product_id asc");

        while($row = mysql_fetch_assoc($queryTransaksiCSV))  
        {  
             fputcsv($output, $row);  
        }  

        fclose($output);  
?>