<?php
    error_reporting(0);
    $id = $_GET['id'];
    $sql = mysql_query("SELECT * FROM store WHERE id = 1");
    $store = mysql_fetch_array($sql);
?>

<?php
if (isset($_POST['simpan'])) {
    if (!empty($_FILES) && $_FILES['product_images']['size'] > 0 && $_FILES['product_images']['error'] == 0) {
        $ext = substr($_FILES['product_images']['name'], strrpos($_FILES['product_images']['name'], '.') + 1);
        $filename =  substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.'  . $ext;

        $move = move_uploaded_file($_FILES['product_images']['tmp_name'], 'assets/images/' . $filename);
        $queryUpdate=mysql_query("UPDATE store SET 
                                name      = '".$_POST['name']."',
                                street     = '".$_POST['street']."',
                                city     = '".$_POST['city']."',
                                province      = '".$_POST['province']."',
                                phone     = '".$_POST['phone']."',
                                logo     = '".$filename."'
                                WHERE id = 1
                                 "); 
    } else {
        $queryUpdate=mysql_query("UPDATE store SET 
                                name      = '".$_POST['name']."',
                                street     = '".$_POST['street']."',
                                city     = '".$_POST['city']."',
                                province      = '".$_POST['province']."',
                                phone     = '".$_POST['phone']."'
                                WHERE id = 1
                                 "); 
    }

    if ($queryUpdate) {
        echo "<script> alert('Data Berhasil Disimpan'); location.href='index.php?hal=store' </script>";
        exit;
    }
}

?>
<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Data Toko
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal adminex-form" method="POST" enctype="multipart/form-data"
                              action="">
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Nama</label>
                                <div class="col-lg-5">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text" value="<?= $store['name'] ?>" required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Jalan</label>
                                <div class="col-lg-5">
                                    <input class=" form-control" id="cname" name="street" minlength="2" value="<?= $store['street'] ?>" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Kota</label>
                                <div class="col-lg-5">
                                    <input class=" form-control" id="cname" name="city" minlength="2" value="<?= $store['city'] ?>" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Provinsi</label>
                                <div class="col-lg-5">
                                    <input class=" form-control" id="cname" name="province" minlength="2" value="<?= $store['province'] ?>" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Telepon</label>
                                <div class="col-lg-5">
                                    <input class=" form-control" id="cname" name="phone" minlength="2" value="<?= $store['phone'] ?>" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2" style="text-align: left;">Foto</label>

                                <!-- master footo startv -->
                                <div class="col-md-5">
                                    <div class="fileupload fileupload-new" data-provides="fileupload"><input
                                                type="hidden">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="assets/images/<?= $store['logo'] ?>"
                                                 alt="">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                        <div>
                                                   <span class="btn btn-default btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                               class="fa fa-undo"></i> Change</span>
                                                   <input type="file" name="product_images" class="default">
                                                   </span>
                                            <a href="#" class="btn btn-danger fileupload-exists"
                                               data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- master foto end -->
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-primary" type="submit" name="simpan">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->
